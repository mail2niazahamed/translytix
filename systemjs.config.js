/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function(global) {
    System.config({

        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            '@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
            '@angular/material': 'npm:@angular/material/material.umd.js',
            // other libraries
            'rxjs': 'npm:rxjs',
            'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            'angular2-datatable': 'node_modules/angular2-datatable',
            'lodash': 'node_modules/lodash/lodash.js',
            'ng2-flex-layout': 'npm:ng2-flex-layout/dist',
            'angular2-highcharts': 'node_modules/angular2-highcharts',
            'highcharts': 'node_modules/highcharts',
            'ng2-resource-rest': 'npm:ng2-resource-rest/bundles/ng2-resource-rest.js',
            'ng2-resource': 'npm:ng2-resource/bundles/index.js',
            "ng2-modal": "npm:ng2-modal",
            'ng2-cookies': 'npm:ng2-cookies',
            'DatepickerModule': 'npm:angular2-material-datepicker',
            "materialize-css": "node_modules/materialize-css",
            "angular2-materialize": "node_modules/angular2-materialize",
            "ng2-pagination": "npm:ng2-pagination"
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            },
            'rxjs': { defaultExtension: 'js' },
            'angular2-datatable': {
                defaultExtension: 'js',
                main: 'index.js'
            },
            'lodash': {
                defaultExtension: 'js',
            },
            'angular2-highcharts': {
                main: './index.js',
                defaultExtension: 'js'
            },
            'highcharts': {
                defaultExtension: 'js',
                main: './highcharts.js'
            },
            'ng2-flex-layout': {
                defaultExtension: 'js',
                'main': 'index.js'
            },
            'jquery': {
                defaultExtension: 'ts',
                'main': 'index.d.ts'
            },
            "ng2-modal": {
                "main": "index.js",
                "defaultExtension": "js"
            },
            'ng2-cookies': {
                main: './ng2-cookies.js',
                defaultExtension: 'js'
            },
            'DatepickerModule': {
                main: 'index.js',
                defaultExtension: 'js'
            },
            'angular2-materialize': {
                main: 'dist/index.js',
                defaultExtension: 'js'
            },
            'materialize-css': {
                format: "global",
                main: "dist/js/materialize",
                defaultExtension: 'js'
            },
             'ng2-pagination': {
                main: "index.js",
                defaultExtension: 'js'
            }
        }
    });
})(this);
