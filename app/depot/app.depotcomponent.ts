import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Headers, Http, RequestOptions } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var swal: any;

@Component({
   selector: 'login-component',
   templateUrl: 'app/depot/app.depot.html',
})

export class DepotComponent {
   //public userDetail: DepotComponent[]
   public halls: DepotComponent[]
   public _data: DepotComponent[]
   public selectedDepot
   hightlightStatus: Array<boolean> = [];
   http;
   promiseDepot = false
   promiseDepotUsers = false
   depotUserTab=false
   constructor(private _http: Http, public toastr: ToastsManager) {
      this.http = this._http
   }
   output: JSON;
   obj: any;
   p: number = 1;
   page = this.p;
   count = 0;
   selDepotName;
   selectedRow = 0;
   userDetail = []
   depot = {}
   user = {}
   depot_sel = {}
   depotUser = {}
   depotUser_sel = {}
   options: Object;
   showAdd = false;
   showDepotUser = false;
   isDepotEdit = false;
   isDepotEditUser = false;
   doc_classes_colors = ["#F44336", "#D32F2F", "#C62828", "#B71C1C", "#FF1744", "#D50000", "#E91E63", "#C2185B", "#AD1457", "#880E4F", "#F50057", "#C51162", "#9C27B0", "#7B1FA2", "#6A1B9A", "#4A148C", "#D500F9", "#AA00FF", "#673AB7", "#512DA8", "#4527A0", "#311B92", "#651FFF", "#6200EA", "#3F51B5", "#303F9F", "#283593", "#1A237E", "#3D5AFE", "#304FFE", "#2196F3", "#1976D2", "#1565C0", "#0D47A1", "#2979FF", "#2962FF", "#03A9F4", "#0288D1", "#0277BD", "#01579B", "#00B0FF", "#0091EA", "#00BCD4", "#0097A7", "#00838F", "#006064", "#00E5FF", "#00B8D4", "#009688", "#00796B", "#00695C", "#004D40", "#1DE9B6", "#00BFA5", "#4CAF50", "#388E3C", "#2E7D32", "#1B5E20", "#00E676", "#00C853", "#8BC34A", "#689F38", "#558B2F", "#33691E", "#76FF03", "#64DD17", "#CDDC39", "#AFB42B", "#9E9D24", "#827717", "#C6FF00", "#AEEA00", "#FFEB3B", "#FBC02D", "#F9A825", "#F57F17", "#FFEA00", "#FFD600", "#FFC107", "#FFA000", "#FF8F00", "#FF6F00", "#FFC400", "#FFAB00", "#FF9800", "#F57C00", "#EF6C00", "#E65100", "#FF9100", "#FF6D00", "#FF5722", "#E64A19", "#D84315", "#BF360C", "#FF3D00", "#DD2C00", "#795548", "#5D4037", "#4E342E", "#3E2723", "#9E9E9E", "#616161", "#424242", "#212121", "#607D8B", "#455A64", "#37474F", "#263238", "#000000", "#F44336"]

   getRandomClr() {
      return Math.round(Math.random() * (110 - 0 + 1)) + 0
   }

   ngOnInit() {
      this.refreshDepots(1);
      let userCookie = Cookie.get('translytixuser');
      if (!userCookie) {
         window.location.assign("/login.html")
      }
   }

   getRandomColor() {
      return {
         background: '#' + Math.floor(Math.random() * 16777215).toString(16)
      }
   }

   reloadPosition() {
      this.showAdd = false; this.isDepotEdit = false; this.depot = {};
   }
   reloadDepotUserPosition() {
      this.showDepotUser = false; this.isDepotEditUser = false; this.depotUser = {};
   }
   onRowClick(event, depotVal, values, $index) {
      var start = event.target.selectionStart;
      var end = event.target.selectionEnd;
      event.target.setSelectionRange(start, end);
      this.halls[$index][depotVal] = event.target.innerHTML.trim();
   }

   editDepot(depot) {
      this.showAdd = true;
      this.depot = depot;
      this.isDepotEdit = true;
   }
   updateDepot(depot) {
      this.promiseDepot = true
      let resultVal = JSON.stringify(depot);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this._http.put("http://35.162.123.99:8080/gl/depot/" + JSON.parse(resultVal).id, JSON.parse(resultVal), options)
         .subscribe(
         data => {
            this._data = data.json();
            this.reloadPosition();
            this.refreshDepots(1);
            this.promiseDepot = false
            this.toastr.success('Depot updated successfully', 'Success!');
         }
         );
   }

   editDepotUser(depotUser) {
      console.log(JSON.stringify(depotUser))
      this.showDepotUser = true;
      this.depotUser = depotUser;
      this.isDepotEditUser = true;
   }

   updateDepotUser(depot) {
      this.promiseDepotUsers = true
      let resultVal = JSON.stringify(depot)
      this.obj =
         { "id": JSON.parse(resultVal).depot_user_id, "name": JSON.parse(resultVal).user_name, "email": JSON.parse(resultVal).user_email, "mobile": JSON.parse(resultVal).user_mobile, "status": 1 };
      this.output = <JSON>this.obj;
      console.log('xx' + this.output);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this._http.put("http://35.162.123.99:8080/gl/depot_user/" + JSON.parse(resultVal).depot_user_id, this.output, options)
         .subscribe(
         data => {
            this._data = data.json();
            this.reloadDepotUserPosition();
            this.fetchDepotUsers(this.depot_sel);
            this.promiseDepotUsers = false
            this.toastr.success('Depot user updated successfully', 'Success!');
         }
         );
   }

   onRowClickUser(depotVal, $index, event) {
      var start = event.target.selectionStart;
      var end = event.target.selectionEnd;
      event.target.setSelectionRange(start, end);
      this.userDetail[$index][depotVal] = event.target.innerHTML.trim();
   }

   updateDepotID() {
      this.depotUser = {
         depot_id: this.selectedDepot
      }
   }

   fetchDepotUsers(depotVal) {

      this.promiseDepotUsers = true
      this.depotUser = {
         depot_id: depotVal[0].id
      }
      // this.depotUser.depot_id=depotVal[0].id
      this.selectedDepot = depotVal[0].id
      this.selDepotName = depotVal[0].name
      this.depot_sel = depotVal;
      this.showDepotUser = false
      if (depotVal) {
         this._http.get('http://35.162.123.99:8080/depots/users?id=' + depotVal[0].id)
            .subscribe(userDetail => {
               this.userDetail = userDetail.json()
               this.promiseDepotUsers = false
            });
      } else {
         this.userDetail = null
      }
   }

   refreshDepots(ev) {
      this.promiseDepot = true
      this.showAdd = false,
         this._http.get('http://35.162.123.99:8080/gl/depot?page=' + ev + '&limit=10')
            .subscribe(halls => {
               this.halls = halls.json().data;
               this.p = parseInt(halls.json().page)
               this.count = halls.json().count
               if (this.halls[0]) {
                  console.log(this.halls);
                  let sel_depot = [];
                  sel_depot.push(this.halls[0]);
                  console.log('2...' + sel_depot);
                  this.fetchDepotUsers(sel_depot);
                  this.promiseDepot = false
               }

            }
            );
   }

   saveDepot(param) {
      this.promiseDepot = true
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = param;
      this._http.post("http://35.162.123.99:8080/gl/depot", param, options)
         .subscribe(
         data => {
            this._data = data.json();
            this.promiseDepot = false
            this.reloadPosition();
            this.refreshDepots(1);
         }
         );
   }


   saveDepotUser(depot) {
      this.promiseDepotUsers = true
      let resultVal = JSON.stringify(depot)
      console.log('aa' + resultVal);

      this.obj =
         { "depot_id": JSON.parse(resultVal).depot_id, "name": JSON.parse(resultVal).user_name, "email": JSON.parse(resultVal).user_email, "mobile": JSON.parse(resultVal).user_mobile, "status": 1 };


      this.output = <JSON>this.obj;
      console.log('xx' + this.output);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this._http.post("http://35.162.123.99:8080/gl/depot_user/", this.output, options)
         .subscribe(
         data => {
            this._data = data.json();
            this.reloadDepotUserPosition();
            this.promiseDepotUsers = false
            this.fetchDepotUsers(this.depot_sel);
         }
         );
   }

   deleteDepots(param) {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = param;
      this._http.delete("http://35.162.123.99:8080/gl/depot/" + param.id, options)
         .subscribe(
         data => {
            this._data = data.json();
            let thisData = JSON.stringify(this._data)
            thisData = JSON.parse(thisData).code
            console.log(this._data);
            this.toastr.success('Depot deleted successfully', 'Success!');
            if (this._data.hasOwnProperty('code')) {
               console.log(thisData);
               swal(
                  'Error!',
                  'Error: ' + thisData,
                  'error'
               );
            } else {
               swal(
                  'Deleted!',
                  'Depot deleted.',
                  'success'
               );
            }
            this.reloadPosition();
            this.refreshDepots(1);
            this.fetchDepotUsers(this.depot_sel);
         }
         );
   }

   callDepotDelete(event, param) {
      let $this = this;
      swal({
         title: 'Are you sure?',
         text: "You won't be able to revert this!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes, delete it!',
      }).then(function() {
         $this.deleteDepots(param)
      })
   }

   deleteDepotUsers(param) {
      let resultVal = JSON.stringify(param);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = param;
      this._http.delete("http://35.162.123.99:8080/gl/depot_user/" + param.depot_user_id, options)
         .subscribe(
         data => {
            this._data = data.json();
            console.log(this._data);
            this.toastr.success('Depot user deleted successfully', 'Success!');
            if (this._data.hasOwnProperty('code')) {
               let thisData = JSON.stringify(this._data)
               thisData = JSON.parse(thisData).code
               swal(
                  'Error!',
                  'Error: ' + thisData,
                  'error'
               );
            } else {
               'Deleted!',
                  'User deleted.',
                  'success'
            }
            this.fetchDepotUsers(this.depot_sel);
         }
         );
   }

   callUserDelete(param) {
      let $this = this;
      swal({
         title: 'Are you sure?',
         text: "You won't be able to revert this!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Yes, delete it!',
      }).then(function() {
         $this.deleteDepotUsers(param)
      })
   }
}
