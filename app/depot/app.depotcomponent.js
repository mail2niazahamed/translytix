"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
var ng2_toastr_1 = require('ng2-toastr/ng2-toastr');
var DepotComponent = (function () {
    function DepotComponent(_http, toastr) {
        this._http = _http;
        this.toastr = toastr;
        this.hightlightStatus = [];
        this.promiseDepot = false;
        this.promiseDepotUsers = false;
        this.depotUserTab = false;
        this.p = 1;
        this.page = this.p;
        this.count = 0;
        this.selectedRow = 0;
        this.userDetail = [];
        this.depot = {};
        this.user = {};
        this.depot_sel = {};
        this.depotUser = {};
        this.depotUser_sel = {};
        this.showAdd = false;
        this.showDepotUser = false;
        this.isDepotEdit = false;
        this.isDepotEditUser = false;
        this.doc_classes_colors = ["#F44336", "#D32F2F", "#C62828", "#B71C1C", "#FF1744", "#D50000", "#E91E63", "#C2185B", "#AD1457", "#880E4F", "#F50057", "#C51162", "#9C27B0", "#7B1FA2", "#6A1B9A", "#4A148C", "#D500F9", "#AA00FF", "#673AB7", "#512DA8", "#4527A0", "#311B92", "#651FFF", "#6200EA", "#3F51B5", "#303F9F", "#283593", "#1A237E", "#3D5AFE", "#304FFE", "#2196F3", "#1976D2", "#1565C0", "#0D47A1", "#2979FF", "#2962FF", "#03A9F4", "#0288D1", "#0277BD", "#01579B", "#00B0FF", "#0091EA", "#00BCD4", "#0097A7", "#00838F", "#006064", "#00E5FF", "#00B8D4", "#009688", "#00796B", "#00695C", "#004D40", "#1DE9B6", "#00BFA5", "#4CAF50", "#388E3C", "#2E7D32", "#1B5E20", "#00E676", "#00C853", "#8BC34A", "#689F38", "#558B2F", "#33691E", "#76FF03", "#64DD17", "#CDDC39", "#AFB42B", "#9E9D24", "#827717", "#C6FF00", "#AEEA00", "#FFEB3B", "#FBC02D", "#F9A825", "#F57F17", "#FFEA00", "#FFD600", "#FFC107", "#FFA000", "#FF8F00", "#FF6F00", "#FFC400", "#FFAB00", "#FF9800", "#F57C00", "#EF6C00", "#E65100", "#FF9100", "#FF6D00", "#FF5722", "#E64A19", "#D84315", "#BF360C", "#FF3D00", "#DD2C00", "#795548", "#5D4037", "#4E342E", "#3E2723", "#9E9E9E", "#616161", "#424242", "#212121", "#607D8B", "#455A64", "#37474F", "#263238", "#000000", "#F44336"];
        this.http = this._http;
    }
    DepotComponent.prototype.getRandomClr = function () {
        return Math.round(Math.random() * (110 - 0 + 1)) + 0;
    };
    DepotComponent.prototype.ngOnInit = function () {
        this.refreshDepots(1);
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        if (!userCookie) {
            window.location.assign("/login.html");
        }
    };
    DepotComponent.prototype.getRandomColor = function () {
        return {
            background: '#' + Math.floor(Math.random() * 16777215).toString(16)
        };
    };
    DepotComponent.prototype.reloadPosition = function () {
        this.showAdd = false;
        this.isDepotEdit = false;
        this.depot = {};
    };
    DepotComponent.prototype.reloadDepotUserPosition = function () {
        this.showDepotUser = false;
        this.isDepotEditUser = false;
        this.depotUser = {};
    };
    DepotComponent.prototype.onRowClick = function (event, depotVal, values, $index) {
        var start = event.target.selectionStart;
        var end = event.target.selectionEnd;
        event.target.setSelectionRange(start, end);
        this.halls[$index][depotVal] = event.target.innerHTML.trim();
    };
    DepotComponent.prototype.editDepot = function (depot) {
        this.showAdd = true;
        this.depot = depot;
        this.isDepotEdit = true;
    };
    DepotComponent.prototype.updateDepot = function (depot) {
        var _this = this;
        this.promiseDepot = true;
        var resultVal = JSON.stringify(depot);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this._http.put("http://35.162.123.99:8080/gl/depot/" + JSON.parse(resultVal).id, JSON.parse(resultVal), options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.reloadPosition();
            _this.refreshDepots(1);
            _this.promiseDepot = false;
            _this.toastr.success('Depot updated successfully', 'Success!');
        });
    };
    DepotComponent.prototype.editDepotUser = function (depotUser) {
        console.log(JSON.stringify(depotUser));
        this.showDepotUser = true;
        this.depotUser = depotUser;
        this.isDepotEditUser = true;
    };
    DepotComponent.prototype.updateDepotUser = function (depot) {
        var _this = this;
        this.promiseDepotUsers = true;
        var resultVal = JSON.stringify(depot);
        this.obj =
            { "id": JSON.parse(resultVal).depot_user_id, "name": JSON.parse(resultVal).user_name, "email": JSON.parse(resultVal).user_email, "mobile": JSON.parse(resultVal).user_mobile, "status": 1 };
        this.output = this.obj;
        console.log('xx' + this.output);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this._http.put("http://35.162.123.99:8080/gl/depot_user/" + JSON.parse(resultVal).depot_user_id, this.output, options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.reloadDepotUserPosition();
            _this.fetchDepotUsers(_this.depot_sel);
            _this.promiseDepotUsers = false;
            _this.toastr.success('Depot user updated successfully', 'Success!');
        });
    };
    DepotComponent.prototype.onRowClickUser = function (depotVal, $index, event) {
        var start = event.target.selectionStart;
        var end = event.target.selectionEnd;
        event.target.setSelectionRange(start, end);
        this.userDetail[$index][depotVal] = event.target.innerHTML.trim();
    };
    DepotComponent.prototype.updateDepotID = function () {
        this.depotUser = {
            depot_id: this.selectedDepot
        };
    };
    DepotComponent.prototype.fetchDepotUsers = function (depotVal) {
        var _this = this;
        this.promiseDepotUsers = true;
        this.depotUser = {
            depot_id: depotVal[0].id
        };
        // this.depotUser.depot_id=depotVal[0].id
        this.selectedDepot = depotVal[0].id;
        this.selDepotName = depotVal[0].name;
        this.depot_sel = depotVal;
        this.showDepotUser = false;
        if (depotVal) {
            this._http.get('http://35.162.123.99:8080/depots/users?id=' + depotVal[0].id)
                .subscribe(function (userDetail) {
                _this.userDetail = userDetail.json();
                _this.promiseDepotUsers = false;
            });
        }
        else {
            this.userDetail = null;
        }
    };
    DepotComponent.prototype.refreshDepots = function (ev) {
        var _this = this;
        this.promiseDepot = true;
        this.showAdd = false,
            this._http.get('http://35.162.123.99:8080/gl/depot?page=' + ev + '&limit=10')
                .subscribe(function (halls) {
                _this.halls = halls.json().data;
                _this.p = parseInt(halls.json().page);
                _this.count = halls.json().count;
                if (_this.halls[0]) {
                    console.log(_this.halls);
                    var sel_depot = [];
                    sel_depot.push(_this.halls[0]);
                    console.log('2...' + sel_depot);
                    _this.fetchDepotUsers(sel_depot);
                    _this.promiseDepot = false;
                }
            });
    };
    DepotComponent.prototype.saveDepot = function (param) {
        var _this = this;
        this.promiseDepot = true;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = param;
        this._http.post("http://35.162.123.99:8080/gl/depot", param, options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.promiseDepot = false;
            _this.reloadPosition();
            _this.refreshDepots(1);
        });
    };
    DepotComponent.prototype.saveDepotUser = function (depot) {
        var _this = this;
        this.promiseDepotUsers = true;
        var resultVal = JSON.stringify(depot);
        console.log('aa' + resultVal);
        this.obj =
            { "depot_id": JSON.parse(resultVal).depot_id, "name": JSON.parse(resultVal).user_name, "email": JSON.parse(resultVal).user_email, "mobile": JSON.parse(resultVal).user_mobile, "status": 1 };
        this.output = this.obj;
        console.log('xx' + this.output);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this._http.post("http://35.162.123.99:8080/gl/depot_user/", this.output, options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.reloadDepotUserPosition();
            _this.promiseDepotUsers = false;
            _this.fetchDepotUsers(_this.depot_sel);
        });
    };
    DepotComponent.prototype.deleteDepots = function (param) {
        var _this = this;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = param;
        this._http.delete("http://35.162.123.99:8080/gl/depot/" + param.id, options)
            .subscribe(function (data) {
            _this._data = data.json();
            var thisData = JSON.stringify(_this._data);
            thisData = JSON.parse(thisData).code;
            console.log(_this._data);
            _this.toastr.success('Depot deleted successfully', 'Success!');
            if (_this._data.hasOwnProperty('code')) {
                console.log(thisData);
                swal('Error!', 'Error: ' + thisData, 'error');
            }
            else {
                swal('Deleted!', 'Depot deleted.', 'success');
            }
            _this.reloadPosition();
            _this.refreshDepots(1);
            _this.fetchDepotUsers(_this.depot_sel);
        });
    };
    DepotComponent.prototype.callDepotDelete = function (event, param) {
        var $this = this;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $this.deleteDepots(param);
        });
    };
    DepotComponent.prototype.deleteDepotUsers = function (param) {
        var _this = this;
        var resultVal = JSON.stringify(param);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = param;
        this._http.delete("http://35.162.123.99:8080/gl/depot_user/" + param.depot_user_id, options)
            .subscribe(function (data) {
            _this._data = data.json();
            console.log(_this._data);
            _this.toastr.success('Depot user deleted successfully', 'Success!');
            if (_this._data.hasOwnProperty('code')) {
                var thisData = JSON.stringify(_this._data);
                thisData = JSON.parse(thisData).code;
                swal('Error!', 'Error: ' + thisData, 'error');
            }
            else {
                'Deleted!',
                    'User deleted.',
                    'success';
            }
            _this.fetchDepotUsers(_this.depot_sel);
        });
    };
    DepotComponent.prototype.callUserDelete = function (param) {
        var $this = this;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $this.deleteDepotUsers(param);
        });
    };
    DepotComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: 'app/depot/app.depot.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http, ng2_toastr_1.ToastsManager])
    ], DepotComponent);
    return DepotComponent;
}());
exports.DepotComponent = DepotComponent;
//# sourceMappingURL=app.depotcomponent.js.map