"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var LaneOpportunitiesComponent = (function () {
    function LaneOpportunitiesComponent(_http) {
        this._http = _http;
        this.noRecords = false;
        this.p = 1;
        this.page = this.p;
        this.count = 0;
    }
    LaneOpportunitiesComponent.prototype.ngOnInit = function () {
        this.getMyBids(1, '');
    };
    LaneOpportunitiesComponent.prototype.fetchLaneDetails = function (laneData) {
        var _this = this;
        this._http.get('http://35.162.123.99:8080/gl/BD_Lane/' + laneData.id)
            .subscribe(function (laneDetails) {
            console.log(laneDetails.json());
            _this.laneDetails = laneDetails.json().data;
        });
    };
    LaneOpportunitiesComponent.prototype.getMyBids = function (ev, param) {
        var _this = this;
        this.page = ev ? ev : 0;
        //param = "' '?"
        param = param == '' ? "' '?" : param + "?";
        this._http.get('http://35.162.123.99:8080/gl/BD_Lane?' + "page=" + this.page + "&limit=10")
            .subscribe(function (details) {
            console.log(details.json());
            _this.details = details.json().data;
        });
    };
    LaneOpportunitiesComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: 'app/laneopportunities/app.laneopportunities.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], LaneOpportunitiesComponent);
    return LaneOpportunitiesComponent;
}());
exports.LaneOpportunitiesComponent = LaneOpportunitiesComponent;
//# sourceMappingURL=app.laneopportunities.js.map