import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http, Headers } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, CanActivate } from '@angular/router';
import myGlobals = require('./globals');
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { MdSidenav } from '@angular/material';
import 'rxjs/Rx';

@Component({
	selector: 'my-app',
	templateUrl: 'app/app.toolbar.html'
})
export class AppComponent {
	constructor(private _router: Router, private _http: Http) { }
	myVar = false;
	myVarBid = false;
	openMode;
	userMenu;
	bid;
	mode = "side"
	selectedRow = 'shipment'
	ngOnInit() {
		let userCookie = Cookie.get('translytixuser');
		let menu = Cookie.get('menu');
		if (menu == 'POD') {
			this.userMenu = 'admin'
		} else if (menu == 'BID') {
			this._router.navigate(['/uploadlanes']);
			this.userMenu = 'BIDuser'
			this.bid = true;
		}
		if (!userCookie) {
			window.location.assign("/login.html")
		}
		if ($(window).width() <= 560) {
			this.openMode = false
			this.mode = "over"
		}
		else {
			this.openMode = true;
			this.mode = "side"
		}
	}

	login(username, password) {
		this._router.navigate(['/dashboard']);
	}

	logout() {
		Cookie.delete('translytixuser');
		window.location.assign('login.html')

	}
	onResize(event) {
		if (event.target.innerWidth <= 560) {
			this.openMode = false;
			this.mode = "over"
		} else {
			this.openMode = true
			this.mode = "side"
		}
	}
}
