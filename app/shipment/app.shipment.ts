import { Component, EventEmitter } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OnInit, ViewEncapsulation } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import 'rxjs/Rx';
import { Pipe, PipeTransform } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
declare var swal: any;

@Component({
	selector: 'login-component',
	templateUrl: 'app/shipment/shipment.html'
})
export class ShipmentComponent {
	public halls: ShipmentComponent[]
	public details: ShipmentComponent[]
	public maxDays: ShipmentComponent[]
	noRecords = false;
	promiseShipment = false;
	promiseShipmentDetails = false;
	selectedRow = 0;
	p: number = 1;
	page = this.p;
	count=0;
	startDt;
	fromDate;
	toDate;
	dtRangeEnabled = false;
	chkBox = true;
	myModel = ''
	maxDaysVal
	showBottomSheet = false
	constructor(private _http: Http) {

	}
	options: Object;
	ngOnInit() {
		let userCookie = Cookie.get('translytixuser');
		if (!userCookie) {
			window.location.assign("/login.html")
		} else {
			this.getMaxDays()
		}
	}

	pageChanged(page) {
		console.log(page)
		this.page++
	}

	getMaxDays() {
		this.promiseShipment = true
		this._http.get('http://35.162.123.99:8080/gl/settings/3')
			.subscribe(maxDays => {
				this.maxDays = maxDays.json()
				this.maxDaysVal = JSON.stringify(this.maxDays)
				this.maxDaysVal = JSON.parse(this.maxDaysVal)
				this.maxDaysVal = this.maxDaysVal[0].svalue
				console.log(this.maxDaysVal)
				let param = ' '
				this.refreshShipments(1, param)
			});
	}

	fetchShipmentDetails(shipmentVal) {
		this.promiseShipmentDetails = true
		this._http.get('http://35.162.123.99:8080/shipmentdetails?id=' + shipmentVal[0].id)
			.subscribe(details => {
				this.details = details.json().length > 0 ? details.json() : (this.noRecords = true),
					this.promiseShipmentDetails = false
			});
	}

	refreshShipments(ev, param) {
		console.log(ev)
		this.page = ev ? ev : 0 
		//param = "' '?"
		param = param == '' ? "' '?" : param +"?"
		if (this.chkBox) {
			param = param + "days=" + this.maxDaysVal + "&"
		}
		this._http.get('http://35.162.123.99:8080/shipments/search/' + param + "page=" + this.page +  "&limit=10")
			.subscribe(halls => {
				this.halls = halls.json().data
				this.p = parseInt(halls.json().page)
				this.count = halls.json().count
				let sel_depot = [];
				sel_depot.push(this.halls[0]);
				this.fetchShipmentDetails(sel_depot);
				this.promiseShipment = false
			});
	}

	searchShipment(param) {
		this.promiseShipment = true
		param = param == '' ? "' '" : param
		if (this.chkBox) {
			param = param + "?days=" + this.maxDaysVal
		}
		this._http.get('http://35.162.123.99:8080/shipments/search/' + param)
			.subscribe(halls => {
				this.halls = halls.json()
				let sel_depot = [];
				sel_depot.push(this.halls[0]);
				console.log('2...' + sel_depot);
				this.promiseShipment = false
				this.fetchShipmentDetails(sel_depot);
			}
			);
	}

	openURL(param) {
		let url = (param)
		window.open("https://pod-prod.s3.amazonaws.com/" + url, "_blank")
	}
}

@Pipe({ name: 'encodeuri' })
export class encodeuri implements PipeTransform {
	transform(value, arg) {
		let url = value
		url = url.replace('#', '%23')
		url = url.replace('#', '%23')
		return url;
	}
}

@Pipe({ name: 'search' })
export class search implements PipeTransform {
	transform(value, args) {
		return args ? value.filter((item) => item.ship_num.includes(args)) : value
	}
}