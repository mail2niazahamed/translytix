"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var material_1 = require('@angular/material');
var app_component_1 = require('./app.component');
var app_dashboardcomponent_1 = require('./dashboard/app.dashboardcomponent');
var app_usercomponent_1 = require('./users/app.usercomponent');
var app_depotcomponent_1 = require('./depot/app.depotcomponent');
var app_mybidscomponent_1 = require('./currentbids/app.mybidscomponent');
var app_managebidders_1 = require('./managebidders/app.managebidders');
var app_uploadlanes_1 = require('./uploadlanes/app.uploadlanes');
var app_laneopportunities_1 = require('./laneopportunities/app.laneopportunities');
var app_reviewbidscomponent_1 = require('./reviewbids/app.reviewbidscomponent');
var app_shipment_1 = require('./shipment/app.shipment');
var router_1 = require('@angular/router');
var angular2_datatable_1 = require("angular2-datatable");
var ng2_flex_layout_1 = require('ng2-flex-layout');
var forms_1 = require('@angular/forms');
var angular2_highcharts_1 = require('angular2-highcharts');
var ng2_resource_rest_1 = require('ng2-resource-rest');
var http_1 = require('@angular/http');
var ng2_modal_1 = require("ng2-modal");
var ng2_toastr_1 = require('ng2-toastr/ng2-toastr');
var index_js_1 = require('../node_modules/angular2-material-datepicker/index.js');
var angular2_materialize_1 = require("angular2-materialize");
var ng2_pagination_1 = require('ng2-pagination'); // <-- import the module
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                material_1.MaterialModule.forRoot(),
                router_1.RouterModule.forRoot([
                    {
                        path: '',
                        component: app_shipment_1.ShipmentComponent,
                        pathMatch: 'full',
                    },
                    {
                        path: 'shipment',
                        component: app_shipment_1.ShipmentComponent,
                        pathMatch: 'full',
                    }, {
                        path: 'users',
                        component: app_usercomponent_1.UsersComponent,
                        pathMatch: 'full',
                    }, {
                        path: 'depot',
                        component: app_depotcomponent_1.DepotComponent,
                        pathMatch: 'full',
                    }, {
                        path: 'mybids',
                        component: app_mybidscomponent_1.MyBidsComponent,
                        pathMatch: 'full',
                    }, {
                        path: 'uploadlanes',
                        component: app_uploadlanes_1.UploadLanes,
                        pathMatch: 'full',
                    }, {
                        path: 'managebidders',
                        component: app_managebidders_1.ManageBidders,
                        pathMatch: 'full',
                    }, {
                        path: 'reviewbids',
                        component: app_reviewbidscomponent_1.ReviewBidsComponent,
                        pathMatch: 'full',
                    }, {
                        path: 'laneopportunities',
                        component: app_laneopportunities_1.LaneOpportunitiesComponent,
                        pathMatch: 'full',
                    }]),
                index_js_1.DatepickerModule,
                angular2_datatable_1.DataTableModule,
                ng2_flex_layout_1.LayoutModule,
                angular2_highcharts_1.ChartModule,
                ng2_resource_rest_1.ResourceModule.forRoot(),
                http_1.HttpModule,
                forms_1.FormsModule,
                ng2_modal_1.ModalModule,
                ng2_toastr_1.ToastModule,
                ng2_pagination_1.Ng2PaginationModule,
            ],
            declarations: [app_component_1.AppComponent, app_laneopportunities_1.LaneOpportunitiesComponent, app_managebidders_1.ManageBidders, app_uploadlanes_1.UploadLanes, app_reviewbidscomponent_1.ReviewBidsComponent, app_dashboardcomponent_1.DashboardComponent, app_mybidscomponent_1.MyBidsComponent, app_shipment_1.ShipmentComponent, app_usercomponent_1.UsersComponent, app_depotcomponent_1.DepotComponent, angular2_materialize_1.MaterializeDirective, app_shipment_1.encodeuri, app_shipment_1.search],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map