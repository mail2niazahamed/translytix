"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var DashboardComponent = (function () {
    function DashboardComponent(_http) {
        this._http = _http;
        this.data = [{
                'ship_num': 'Manikandan',
                'depot': 'mai@gmail.com',
                'delivery': '29',
                'customer': 'Chennai',
                'upload_dt': '2016-11-07',
                'upload_mode': 'Mobile'
            }, {
                'ship_num': 'Mani',
                'depot': 'mani@gmail.com',
                'delivery': '25',
                'customer': 'Trichy',
                'upload_dt': '2016-11-07',
                'upload_mode': 'Mobile'
            }];
        this.options = {
            chart: {
                type: 'pie'
            },
            title: { text: '' },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            series: [{
                    name: 'Browsers',
                    data: [["Firefox", 6], ["MSIE", 4], ["Chrome", 7]],
                    size: '60%',
                    innerSize: '20%',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false
                    }
                }]
        };
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this._http.get('http://35.162.123.99:8080/users')
            .map(function (resp) { return resp.json(); })
            .subscribe(function (data) { return console.log(data); });
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/dashboard/app.dashboard.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=app.dashboardcomponent.js.map