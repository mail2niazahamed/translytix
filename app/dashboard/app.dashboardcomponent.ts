import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from '../app.component';
import { HttpModule, Http } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import myGlobals = require('../globals');
import { Cookie } from 'ng2-cookies/ng2-cookies';
  import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import 'rxjs/Rx';
@Component({
  selector: 'my-app',
  templateUrl: 'app/dashboard/app.dashboard.html',
})

export class DashboardComponent {
  public halls: DashboardComponent[]
  constructor(private _http: Http, public toastr: ToastsManager) {
    this.options = {
      chart: {
        type: 'pie'
      },
      title: { text: '' },
      plotOptions: {
        pie: {
          shadow: false,
          center: ['50%', '50%']
        }
      },
      series: [{
        name: 'Browsers',
        data: [["Firefox", 6], ["MSIE", 4], ["Chrome", 7]],
        size: '60%',
        innerSize: '20%',
        showInLegend: true,
        dataLabels: {
          enabled: false
        }
      }]
    };
  }
  options: Object;
  ngOnInit() {  
    this.refreshDepots();
     let userCookie = Cookie.get('translytixuser');
      if(!userCookie){
         window.location.assign("/login.html")
      }
  }
  

  refreshDepots() {
    this._http.get('http://35.162.123.99:8080/gl/shipment')
      .subscribe(halls => this.halls = halls.json()
      );
  }
}
