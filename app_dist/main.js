"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_module_1 = require('./app.module');
var core_1 = require('@angular/core');
require("materialize-css");
require("angular2-materialize");
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
platform.bootstrapModule(app_module_1.AppModule);
if (process.env.ENV === 'production') {
    core_1.enableProdMode();
}
//# sourceMappingURL=main.js.map