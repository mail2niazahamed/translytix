"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
require('rxjs/Rx');
var core_2 = require('@angular/core');
var ShipmentComponent = (function () {
    function ShipmentComponent(_http) {
        this._http = _http;
        this.noRecords = false;
        this.promiseShipment = false;
        this.promiseShipmentDetails = false;
        this.selectedRow = 0;
        this.p = 1;
        this.page = this.p;
        this.count = 0;
        this.dtRangeEnabled = false;
        this.chkBox = true;
        this.myModel = '';
        this.showBottomSheet = false;
    }
    ShipmentComponent.prototype.ngOnInit = function () {
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        if (!userCookie) {
            window.location.assign("/login.html");
        }
        else {
            this.getMaxDays();
        }
    };
    ShipmentComponent.prototype.pageChanged = function (page) {
        console.log(page);
        this.page++;
    };
    ShipmentComponent.prototype.getMaxDays = function () {
        var _this = this;
        this.promiseShipment = true;
        this._http.get('http://35.162.123.99:8080/gl/settings/3')
            .subscribe(function (maxDays) {
            _this.maxDays = maxDays.json();
            _this.maxDaysVal = JSON.stringify(_this.maxDays);
            _this.maxDaysVal = JSON.parse(_this.maxDaysVal);
            _this.maxDaysVal = _this.maxDaysVal[0].svalue;
            console.log(_this.maxDaysVal);
            var param = ' ';
            _this.refreshShipments(1, param);
        });
    };
    ShipmentComponent.prototype.fetchShipmentDetails = function (shipmentVal) {
        var _this = this;
        this.promiseShipmentDetails = true;
        this._http.get('http://35.162.123.99:8080/shipmentdetails?id=' + shipmentVal[0].id)
            .subscribe(function (details) {
            _this.details = details.json().length > 0 ? details.json() : (_this.noRecords = true),
                _this.promiseShipmentDetails = false;
        });
    };
    ShipmentComponent.prototype.refreshShipments = function (ev, param) {
        var _this = this;
        console.log(ev);
        this.page = ev ? ev : 0;
        //param = "' '?"
        param = param == '' ? "' '?" : param + "?";
        if (this.chkBox) {
            param = param + "days=" + this.maxDaysVal + "&";
        }
        this._http.get('http://35.162.123.99:8080/shipments/search/' + param + "page=" + this.page + "&limit=10")
            .subscribe(function (halls) {
            _this.halls = halls.json().data;
            _this.p = parseInt(halls.json().page);
            _this.count = halls.json().count;
            var sel_depot = [];
            sel_depot.push(_this.halls[0]);
            _this.fetchShipmentDetails(sel_depot);
            _this.promiseShipment = false;
        });
    };
    ShipmentComponent.prototype.searchShipment = function (param) {
        var _this = this;
        this.promiseShipment = true;
        param = param == '' ? "' '" : param;
        if (this.chkBox) {
            param = param + "?days=" + this.maxDaysVal;
        }
        this._http.get('http://35.162.123.99:8080/shipments/search/' + param)
            .subscribe(function (halls) {
            _this.halls = halls.json();
            var sel_depot = [];
            sel_depot.push(_this.halls[0]);
            console.log('2...' + sel_depot);
            _this.promiseShipment = false;
            _this.fetchShipmentDetails(sel_depot);
        });
    };
    ShipmentComponent.prototype.openURL = function (param) {
        var url = (param);
        window.open("https://pod-prod.s3.amazonaws.com/" + url, "_blank");
    };
    ShipmentComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: './shipment.html'
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ShipmentComponent);
    return ShipmentComponent;
}());
exports.ShipmentComponent = ShipmentComponent;
var encodeuri = (function () {
    function encodeuri() {
    }
    encodeuri.prototype.transform = function (value, arg) {
        var url = value;
        url = url.replace('#', '%23');
        url = url.replace('#', '%23');
        return url;
    };
    encodeuri = __decorate([
        core_2.Pipe({ name: 'encodeuri' }), 
        __metadata('design:paramtypes', [])
    ], encodeuri);
    return encodeuri;
}());
exports.encodeuri = encodeuri;
var search = (function () {
    function search() {
    }
    search.prototype.transform = function (value, args) {
        return args ? value.filter(function (item) { return item.ship_num.includes(args); }) : value;
    };
    search = __decorate([
        core_2.Pipe({ name: 'search' }), 
        __metadata('design:paramtypes', [])
    ], search);
    return search;
}());
exports.search = search;
//# sourceMappingURL=app.shipment.js.map