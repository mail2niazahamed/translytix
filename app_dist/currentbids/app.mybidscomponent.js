"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('rxjs/Rx');
var MyBidsComponent = (function () {
    function MyBidsComponent() {
    }
    MyBidsComponent.prototype.ngOnInit = function () {
        if ($(window).width() <= 560) {
            $("#testDel").css("display", "none");
        }
        else {
            $("#testDel").css("display", "block");
        }
    };
    MyBidsComponent.prototype.onResize = function (event) {
        if (event.target.innerWidth <= 560) {
            $("#testDel").css("display", "none");
        }
        else {
            $("#testDel").css("display", "block");
        }
    };
    MyBidsComponent.prototype.onSwipeLeft = function (ev) {
        $("#test").css("margin-left", "-50px");
        $("#testDel").css("display", "block");
    };
    MyBidsComponent.prototype.onSwipeRight = function (ev) {
        $("#test").css("margin-left", "0px");
        $("#testDel").css("display", "none");
    };
    MyBidsComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: './app.currentbids.html',
        }), 
        __metadata('design:paramtypes', [])
    ], MyBidsComponent);
    return MyBidsComponent;
}());
exports.MyBidsComponent = MyBidsComponent;
//# sourceMappingURL=app.mybidscomponent.js.map