"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var myGlobals = require('../globals');
require('rxjs/Rx');
var LoginComponent = (function () {
    function LoginComponent(router, _http) {
        this.router = router;
        this._http = _http;
        this.isVisible = myGlobals.sidenav;
    }
    LoginComponent.prototype.login = function (username, password) {
        var _this = this;
        // alert(username)
        var creds = { useremail: username, pwd: password };
        this._http.get('http://35.162.123.99:8080/users/login?useremail=' + username + '&pwd=' + password)
            .subscribe(function (userDetail) {
            _this.userDetail = userDetail.json(),
                userDetail.json() == '' ? alert('Invalid username and password.') : _this.router.navigate(['/dashboard']),
                myGlobals.sidenav = true;
        });
    };
    LoginComponent.prototype.authenticate = function () {
        this.router.navigate(['/dashboard']);
        var isVisible;
        isVisible = false;
        /* let creds = JSON.stringify({ username: username.value, password: password.value });
         let headers = new Headers();
         headers.append('Content-Type', 'application/json');
         
         this._http.post('http://35.162.123.99:8080/login', creds, {
           headers: headers
         })
           .subscribe(
           data => {
           this.saveJwt(data.json().id_token);
           username.value = null;
           password.value = null;
           },
           err => console.log(err.json().message),
           () => console.log('Authentication Complete')
           );*/
    };
    LoginComponent.prototype.saveJwt = function (jwt) {
        if (jwt) {
            localStorage.setItem('id_token', jwt);
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/login/app.login.html',
        }), 
        __metadata('design:paramtypes', [router_1.Router, http_1.Http])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=app.logincomponent.js.map