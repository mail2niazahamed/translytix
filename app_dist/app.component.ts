import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http, Headers } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, CanActivate } from '@angular/router';
import myGlobals = require('./globals');
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { MdSidenav } from '@angular/material';
import 'rxjs/Rx';

@Component({
	selector: 'my-app',
	templateUrl: 'app.toolbar.html'
})
export class AppComponent {
	constructor(private router: Router, private _http: Http) { }
	myVar = false;
	openMode;
	mode = "side"
	selectedRow = 'shipment'
	ngOnInit() {
		let userCookie = Cookie.get('translytixuser');
		if (!userCookie) {
			window.location.assign("/login.html")
		}
		if ($(window).width() <= 560) {
			this.openMode = false
			this.mode = "over"
		}
		else {
			this.openMode = true;
			this.mode = "side"
		}
	}

	login(username, password) {
		this.router.navigate(['/dashboard']);
	}

	logout() {
		Cookie.delete('translytixuser');
		window.location.assign('login.html')

	}
	onResize(event) {
		if (event.target.innerWidth <= 560) {
			this.openMode = false;
			this.mode = "over"
		} else {
			this.openMode = true
			this.mode = "side"
		}
	}
}
