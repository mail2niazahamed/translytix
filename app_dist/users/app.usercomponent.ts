import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Headers, Http, RequestOptions } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var swal: any;

@Component({
  selector: 'my-app',
  templateUrl: 'app.users.html',
})

export class UsersComponent {
  public halls: UsersComponent[]
  public _data: UsersComponent[]
  user = {}
   doc_classes_colors = ["#F44336", "#D32F2F", "#C62828", "#B71C1C", "#FF1744", "#D50000", "#E91E63", "#C2185B", "#AD1457", "#880E4F", "#F50057", "#C51162", "#9C27B0", "#7B1FA2", "#6A1B9A", "#4A148C", "#D500F9", "#AA00FF", "#673AB7", "#512DA8", "#4527A0", "#311B92", "#651FFF", "#6200EA", "#3F51B5", "#303F9F", "#283593", "#1A237E", "#3D5AFE", "#304FFE", "#2196F3", "#1976D2", "#1565C0", "#0D47A1", "#2979FF", "#2962FF", "#03A9F4", "#0288D1", "#0277BD", "#01579B", "#00B0FF", "#0091EA", "#00BCD4", "#0097A7", "#00838F", "#006064", "#00E5FF", "#00B8D4", "#009688", "#00796B", "#00695C", "#004D40", "#1DE9B6", "#00BFA5", "#4CAF50", "#388E3C", "#2E7D32", "#1B5E20", "#00E676", "#00C853", "#8BC34A", "#689F38", "#558B2F", "#33691E", "#76FF03", "#64DD17", "#CDDC39", "#AFB42B", "#9E9D24", "#827717", "#C6FF00", "#AEEA00", "#FFEB3B", "#FBC02D", "#F9A825", "#F57F17", "#FFEA00", "#FFD600", "#FFC107", "#FFA000", "#FF8F00", "#FF6F00", "#FFC400", "#FFAB00", "#FF9800", "#F57C00", "#EF6C00", "#E65100", "#FF9100", "#FF6D00", "#FF5722", "#E64A19", "#D84315", "#BF360C", "#FF3D00", "#DD2C00", "#795548", "#5D4037", "#4E342E", "#3E2723", "#9E9E9E", "#616161", "#424242", "#212121", "#607D8B", "#455A64", "#37474F", "#263238", "#000000", "#F44336"]

   getRandomClr() {
      return Math.round(Math.random() * (110 - 0 + 1)) + 0
   }
  constructor(private _http: Http, public toastr: ToastsManager) { }
  options: Object;
  showAddUser = false
  isUserEdit = false
  promiseUsers = false
  p: number = 1;
  page = this.p;
  count=0;
  ngOnInit() {
    let userCookie = Cookie.get('translytixuser');
    if (!userCookie) {
      window.location.assign("/login.html")
    } else {
      this.refreshUsers(1)
    }
  }

  callUserDelete(param) {
    let $this = this;
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(function() {
      $this.deleteUsers(param)
    })
  }

  refreshUsers(page) {
    this.page = page
    this.showAddUser = false
    this.promiseUsers = true
    this._http.get('http://35.162.123.99:8080/gl/users?page=' + this.page + "&limit=10")
      .subscribe(halls => {
        this.halls = halls.json().data,
         this.p = parseInt(halls.json().page)
        this.count = halls.json().count
        this.promiseUsers = false
      }
      );
  }

  saveUser(param) {
    //console.log(param.role)
    this.promiseUsers = true
    if (param.role == true) {
      param.role = 'ADMIN'
    } else {
      param.role = 'USER'
    }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = param;
    this._http.post("http://35.162.123.99:8080/gl/users", param, options)
      .subscribe(
      data => {
        this._data = data.json();
        this.toastr.success('User added successfully', 'Success!');
        this.refreshUsers(1)
        this.promiseUsers = false
      }
      );
  }

  showUpdateUser(param) {
    this.showAddUser = true
    this.user = {
      'id': param.id,
      'name': param.name,
      'email': param.email,
      'mobile': param.mobile,
      'pwd': param.pwd,
      'role': param.role == 'ADMIN' ? true : false
    }
    // this.user = param
    this.isUserEdit = true
  }

  reloadPosition() {
    this.showAddUser = false
  }

  emptyUser() {
    this.isUserEdit = false
    this.user = {}
  }

  updateUser(param) {
    this.promiseUsers = true
    if (param.role == true) {
      param.role = 'ADMIN'
    } else {
      param.role = 'USER'
    }
    let resultVal = JSON.stringify(param);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this._http.put("http://35.162.123.99:8080/gl/users/" + JSON.parse(resultVal).id, JSON.parse(resultVal), options)
      .subscribe(
      data => {
        this._data = data.json();
        this.toastr.success('User updated successfully', 'Success!');
        this.refreshUsers(1),
          this.promiseUsers = false
      });
  }

  deleteUsers(user) {
    let resultVal = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this._http.delete("http://35.162.123.99:8080/gl/users/" + JSON.parse(resultVal).id, options)
      .subscribe(
      data => {
        this._data = data.json();
        this.toastr.success('User deleted successfully', 'Success!');
        this.refreshUsers(1);
      }
      );
  }
}
