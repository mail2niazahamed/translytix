"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
require('rxjs/Rx');
var ng2_toastr_1 = require('ng2-toastr/ng2-toastr');
var UsersComponent = (function () {
    function UsersComponent(_http, toastr) {
        this._http = _http;
        this.toastr = toastr;
        this.user = {};
        this.doc_classes_colors = ["#F44336", "#D32F2F", "#C62828", "#B71C1C", "#FF1744", "#D50000", "#E91E63", "#C2185B", "#AD1457", "#880E4F", "#F50057", "#C51162", "#9C27B0", "#7B1FA2", "#6A1B9A", "#4A148C", "#D500F9", "#AA00FF", "#673AB7", "#512DA8", "#4527A0", "#311B92", "#651FFF", "#6200EA", "#3F51B5", "#303F9F", "#283593", "#1A237E", "#3D5AFE", "#304FFE", "#2196F3", "#1976D2", "#1565C0", "#0D47A1", "#2979FF", "#2962FF", "#03A9F4", "#0288D1", "#0277BD", "#01579B", "#00B0FF", "#0091EA", "#00BCD4", "#0097A7", "#00838F", "#006064", "#00E5FF", "#00B8D4", "#009688", "#00796B", "#00695C", "#004D40", "#1DE9B6", "#00BFA5", "#4CAF50", "#388E3C", "#2E7D32", "#1B5E20", "#00E676", "#00C853", "#8BC34A", "#689F38", "#558B2F", "#33691E", "#76FF03", "#64DD17", "#CDDC39", "#AFB42B", "#9E9D24", "#827717", "#C6FF00", "#AEEA00", "#FFEB3B", "#FBC02D", "#F9A825", "#F57F17", "#FFEA00", "#FFD600", "#FFC107", "#FFA000", "#FF8F00", "#FF6F00", "#FFC400", "#FFAB00", "#FF9800", "#F57C00", "#EF6C00", "#E65100", "#FF9100", "#FF6D00", "#FF5722", "#E64A19", "#D84315", "#BF360C", "#FF3D00", "#DD2C00", "#795548", "#5D4037", "#4E342E", "#3E2723", "#9E9E9E", "#616161", "#424242", "#212121", "#607D8B", "#455A64", "#37474F", "#263238", "#000000", "#F44336"];
        this.showAddUser = false;
        this.isUserEdit = false;
        this.promiseUsers = false;
        this.p = 1;
        this.page = this.p;
        this.count = 0;
    }
    UsersComponent.prototype.getRandomClr = function () {
        return Math.round(Math.random() * (110 - 0 + 1)) + 0;
    };
    UsersComponent.prototype.ngOnInit = function () {
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        if (!userCookie) {
            window.location.assign("/login.html");
        }
        else {
            this.refreshUsers(1);
        }
    };
    UsersComponent.prototype.callUserDelete = function (param) {
        var $this = this;
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
        }).then(function () {
            $this.deleteUsers(param);
        });
    };
    UsersComponent.prototype.refreshUsers = function (page) {
        var _this = this;
        this.page = page;
        this.showAddUser = false;
        this.promiseUsers = true;
        this._http.get('http://35.162.123.99:8080/gl/users?page=' + this.page + "&limit=10")
            .subscribe(function (halls) {
            _this.halls = halls.json().data,
                _this.p = parseInt(halls.json().page);
            _this.count = halls.json().count;
            _this.promiseUsers = false;
        });
    };
    UsersComponent.prototype.saveUser = function (param) {
        var _this = this;
        //console.log(param.role)
        this.promiseUsers = true;
        if (param.role == true) {
            param.role = 'ADMIN';
        }
        else {
            param.role = 'USER';
        }
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        var body = param;
        this._http.post("http://35.162.123.99:8080/gl/users", param, options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.toastr.success('User added successfully', 'Success!');
            _this.refreshUsers(1);
            _this.promiseUsers = false;
        });
    };
    UsersComponent.prototype.showUpdateUser = function (param) {
        this.showAddUser = true;
        this.user = {
            'id': param.id,
            'name': param.name,
            'email': param.email,
            'mobile': param.mobile,
            'pwd': param.pwd,
            'role': param.role == 'ADMIN' ? true : false
        };
        // this.user = param
        this.isUserEdit = true;
    };
    UsersComponent.prototype.reloadPosition = function () {
        this.showAddUser = false;
    };
    UsersComponent.prototype.emptyUser = function () {
        this.isUserEdit = false;
        this.user = {};
    };
    UsersComponent.prototype.updateUser = function (param) {
        var _this = this;
        this.promiseUsers = true;
        if (param.role == true) {
            param.role = 'ADMIN';
        }
        else {
            param.role = 'USER';
        }
        var resultVal = JSON.stringify(param);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this._http.put("http://35.162.123.99:8080/gl/users/" + JSON.parse(resultVal).id, JSON.parse(resultVal), options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.toastr.success('User updated successfully', 'Success!');
            _this.refreshUsers(1),
                _this.promiseUsers = false;
        });
    };
    UsersComponent.prototype.deleteUsers = function (user) {
        var _this = this;
        var resultVal = JSON.stringify(user);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        this._http.delete("http://35.162.123.99:8080/gl/users/" + JSON.parse(resultVal).id, options)
            .subscribe(function (data) {
            _this._data = data.json();
            _this.toastr.success('User deleted successfully', 'Success!');
            _this.refreshUsers(1);
        });
    };
    UsersComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app.users.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http, ng2_toastr_1.ToastsManager])
    ], UsersComponent);
    return UsersComponent;
}());
exports.UsersComponent = UsersComponent;
//# sourceMappingURL=app.usercomponent.js.map