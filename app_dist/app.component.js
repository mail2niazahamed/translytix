"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
require('rxjs/Rx');
var AppComponent = (function () {
    function AppComponent(router, _http) {
        this.router = router;
        this._http = _http;
        this.myVar = false;
        this.mode = "side";
        this.selectedRow = 'shipment';
    }
    AppComponent.prototype.ngOnInit = function () {
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        if (!userCookie) {
            window.location.assign("/login.html");
        }
        if ($(window).width() <= 560) {
            this.openMode = false;
            this.mode = "over";
        }
        else {
            this.openMode = true;
            this.mode = "side";
        }
    };
    AppComponent.prototype.login = function (username, password) {
        this.router.navigate(['/dashboard']);
    };
    AppComponent.prototype.logout = function () {
        ng2_cookies_1.Cookie.delete('translytixuser');
        window.location.assign('login.html');
    };
    AppComponent.prototype.onResize = function (event) {
        if (event.target.innerWidth <= 560) {
            this.openMode = false;
            this.mode = "over";
        }
        else {
            this.openMode = true;
            this.mode = "side";
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app.toolbar.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, http_1.Http])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map